<?php
require_once "db.php";
require_once "header.php";
$id = $_GET['product_id'];
$queryString = "SELECT id, name, description, price, in_stack, Picture FROM product WHERE 1=1 AND id = $id";
$queryResult = [];
$queryResult[] = $db->query($queryString)->fetch_all(MYSQLI_ASSOC);


$cut = 'X:/utilites/programs/openserver/OSPanel/domains/fishop/';
$new_str = str_replace($cut, '', $queryResult[0][0]['Picture']);
$count = $_GET['count'];
if ($id and $count){
    $db->query("INSERT INTO cart (product_id, count) VALUE ($id, $count)");
}
?>
<div class="container">
    <div class="row">
        <div class="col-sm-8">

            <img class="img-fluid border border-secondary rounded" src="<?php echo $new_str ?>" alt="Picture">
            <h3><?php echo $queryResult[0][0]['name'] ?></h3>
            <p>Описание: <?php echo $queryResult[0][0]['description'] ?></p>
            <p>Цена: <?php echo $queryResult[0][0]['price'] ?></p>
            <p>На складе: <?php echo $queryResult[0][0]['in_stack'] ?></p>
        </div>
        <div class="col-sm-4">
            <form action="" class="flex-column" style="margin-top: -250px">
                <input data-price="<?php echo $queryResult[0][0]['price']?>" data-name="<?php echo $queryResult[0][0]['name']?>" data-id="<?php echo $queryResult[0][0]['id']?>" type="submit" value="Добавить в корзину" class="btn btn-primary"><br>
                <input id="elem" name="count" value="1" class="form-control form-control-sm" type="number" placeholder="Количество">
                <input type="hidden" name="product_id" value="<?php echo htmlspecialchars($id); ?>">
            </form>
        </div>
    </div>
</div>
    <script>
        $('.btn').click(function (e) {
            e.preventDefault();

            var productId = $(this).data('id');
            var productName = $(this).data('name');
            var productPrice = $(this).data('price');
            var a = document.getElementById('elem');
            var count = parseInt(a.value);

            //console.log(localStorage.getItem('cart'));
            //console.log(localStorage.getItem('prodid'));


            var cart = getCartData() || {};

            if(cart.hasOwnProperty(productId)) {
                cart[productId].cnt += count;
            } else {
                var item = {
                    name : productName,
                    id  : productId,
                    cnt : count,
                    price: productPrice
                };
                cart[productId] = item;
            }

            setCartData(cart);
            /**
             * @returns {any}
             */
            function getCartData() {
                return JSON.parse(localStorage.getItem('cart'));
            }

            function setCartData(cart) {
                return localStorage.setItem('cart', JSON.stringify(cart));
            }

            var arr = getCartData();

        });



    </script>
<?php
require_once "footer.php";
