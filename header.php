<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style/bootstrap.min.css">
    <link rel="stylesheet" href="style/style.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Poiret+One&amp;subset=cyrillic"
          rel="stylesheet">
    <title>Fishop</title>
</head>
<body>
<script src="js/jquery-3.3.1.min.js"></script>
<div class="container" id="header">
    <div class="row">
        <div class="col-sm-3">
            <a href="index.php"><img class="img-fluid" src="img/fish-logo.png" alt="fish-logo"></a>
        </div>

        <div class="col-sm-4">
            <p id="text-logo" class="align-items-center">Fi<img id="S" src="img/S.png" alt="S">hop</p>
        </div>

        <div class="col-sm-5" id="tel">
            <div><p>Тел.: +375 (29) 289 1008</p></div>
            <div><p>Время раб. пн-пт: 9:00 - 21:00</p></div>

        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-9" id="content"  ">



