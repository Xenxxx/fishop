<?php
require_once "db.php";
require_once "header.php";


$queryStr = "SELECT id, name, price, Picture FROM product WHERE 1=1";

//Фильтр по ценам
$from = $_GET['from'] ?: null;
$to = $_GET['to'] ?: null;


if ($_GET['from'] and $_GET['to']) {
    $queryStr .= " AND price > $from and price < $to";
}

if ($_GET['from']) {
    $queryStr .= " AND price > $from";
}

if ($_GET['to']) {
    $queryStr .= " AND price < $to";
}

//категория
$category_id = $_GET['id'];
if ($_GET['id']) {
    $queryStr .= " AND category_id = $category_id";
}

//поиск
$search = $_GET['search'];
if ($search) {
    $queryStr .= sprintf(' AND name LIKE "%%%s%%"', $search);
}

//выпадающий список
$asc = $_GET['sort'];
if ($asc == 'asc') {
    $queryStr .= " ORDER BY name ASC";
} elseif ($asc == 'desc') {
    $queryStr .= " ORDER BY name DESC";
} elseif ($asc == 'ascPrice') {
    $queryStr .= " ORDER BY price ASC";
} elseif ($asc == 'descPrice') {
    $queryStr .= " ORDER BY price DESC";
}

$page = $_GET['page'];

function pagination($allProd)
{
    if (isset($_GET['page'])) $page = ($_GET['page'] - 1); else $page = 0;

}


//запрос на фильтрацию
$queryResult = [];
$queryResult[] = $db->query($queryStr);


//обычный запрос
$queryProducts = [];
if (!$_GET) {
    $queryProducts[] = $db->query("SELECT id, name, price, Picture FROM product ORDER BY id DESC")->fetch_all(MYSQLI_ASSOC);
}
?>

    <form action="catalog.php?id=<?php echo $category_id ?>">
        <input type="text" name="search" placeholder="Поиск товара">
        <select name="sort">
            <option name="">-</option>
            <option value="asc">От А до Я</option>
            <option value="desc">От Я до А</option>
            <option value="ascPrice">От меньшей цены</option>
            <option value="descPrice">От большей цены</option>
        </select>
        <input type="number" name="from" placeholder="Цена от">
        <input type="number" name="to" placeholder="Цена до">
        <input type="hidden" name="id" value="<?php echo htmlspecialchars($category_id); ?>">
        <input type="submit" value="Фильтр">
    </form>
<?php
if (!$_GET) {
    foreach ($queryProducts as $item) {
        foreach ($item as $products) {
            $cut = 'X:/utilites/programs/openserver/OSPanel/domains/fishop/';
            $new_str = str_replace($cut, '', $products['Picture']);
            $id = $products['id'];
            ?>

            <div class="card" style="width: 14rem;">
                <img class="card - img - top" src=" <?= $new_str ?>" alt="Изображение товара">
                <div class="card-body">
                    <a style="color: black" href="view_product.php?product_id=<?php echo $id ?>"><h5
                                class="card-title"><?= $products['name'] ?></h5></a>
                    <p class="card-text">Цена: <?= $products['price'] ?></p>
                    <a href="#" class="btn btn-dark" data-name="<?php echo $products['name'] ?>" data-id="<?php echo $id ?>">Добавить в корзину</a>
                </div>
            </div>
            <?php
        }
    }
}
if ($_GET) {
    foreach ($queryResult as $item) {
        foreach ($item as $products) {
            $cut = 'X:/utilites/programs/openserver/OSPanel/domains/fishop/';
            $new_str = str_replace($cut, '', $products['Picture']);
            $id = $products['id'];
            echo $id;
            ?>

            <div class="card" style="width: 14rem;">
                <img class="card-img-top" src="<?= $new_str ?>" alt="Изображение товара">
                <div class="card-body">
                    <a style="color: black" href="view_product.php?product_id=<?php echo $id ?>"><h5
                                class="card-title"><?= $products['name'] ?></h5></a>
                    <p class="card-text">Цена: <?= $products['price'] ?></p>
                    <a href="#" class="btn btn-dark" data-price="<?php echo $products['price']?>" data-name="<?php echo $products['name'] ?>" data-id="<?php echo $id ?>">Добавить в корзину</a>
                </div>
            </div>
            <?php
        }
    }
}

?>
    <script>
        $('.btn').click(function (e) {
            e.preventDefault();

            var productId = $(this).data('id');
            var productName = $(this).data('name');
            var productPrice = $(this).data('price');
            console.log(productId);


            console.log(localStorage.getItem('cart'));
            console.log(localStorage.getItem('prodid'));


            var cart = getCartData() || {};

            if(cart.hasOwnProperty(productId)) {
                cart[productId].cnt += 1;
            } else {
                var item = {
                    name : productName,
                    id  : productId,
                    cnt : 1,
                    price: productPrice
                };
                cart[productId] = item;
            }

            setCartData(cart);
            /**
             * @returns {any}
             */
            function getCartData() {
                return JSON.parse(localStorage.getItem('cart'));
            }

            function setCartData(cart) {
                return localStorage.setItem('cart', JSON.stringify(cart));
            }

        });

    </script>

<?php
require_once "footer.php";