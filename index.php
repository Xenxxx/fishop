<?php
require_once "header.php";
require_once "db.php";
$queryProducts = [];

$queryProducts[] = $db->query("SELECT id, name, price, Picture  FROM product ORDER BY id DESC LIMIT 9")->fetch_all(MYSQLI_ASSOC);


?>
    <h3>Последние товары поступившие в наш магазин:</h3>
<?php
foreach ($queryProducts as $item) {
    foreach ($item as $products) {
        $cut = 'X:/utilites/programs/openserver/OSPanel/domains/fishop/';
        $new_str = str_replace($cut, '', $products['Picture']);
        $id = $products['id'];
        ?>

        <div class="card" style="width: 14rem;">
            <img class="card-img-top" src="<?= $new_str ?>" alt="Изображение товара">
            <div class="card-body">
                <a style="color: black" href="view_product.php?product_id=<?php echo $id ?>"><h5 class="card-title"><?= $products['name'] ?></h5></a>
                <p class="card-text">Цена: <?= $products['price'] ?></p>
                <a href="#" class="btn btn-dark" data-price="<?php echo $products['price']?>" data-name="<?php echo $products['name']?>" data-id="<?php echo $id?>">Добавить в корзину</a>
            </div>
        </div>
        <?php
    }
}
?>

    <script>
        $('.btn').click(function (e) {
            e.preventDefault();

            var productId = $(this).data('id');
            var productName = $(this).data('name');
            var productPrice = $(this).data('price');
            console.log(productId);


            console.log(localStorage.getItem('cart'));
            console.log(localStorage.getItem('prodid'));


            var cart = getCartData() || {};

            if(cart.hasOwnProperty(productId)) {
                cart[productId].cnt += 1;
            } else {
                var item = {
                    name : productName,
                    id  : productId,
                    cnt : 1,
                    price: productPrice
                };
                cart[productId] = item;
            }

            setCartData(cart);
            /**
             * @returns {any}
             */
            function getCartData() {
                return JSON.parse(localStorage.getItem('cart'));
            }

            function setCartData(cart) {
                return localStorage.setItem('cart', JSON.stringify(cart));
            }

        });

    </script>
<?php
require_once "footer.php";